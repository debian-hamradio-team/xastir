
Using native AX.25 on Linux
===========================

There are two ways of installing Xastir on Debian:

1. Installing Xastir so that it must be run as root in order to allow the use
   of native AX.25 interfaces in Linux. Only root will be able to use these
   interfaces but other users may still use serial TNCs or APRS-IS servers.

   This is the default on Debian systems.

2. Members of the xastir-ax25 group will be able to use native AX.25 interfaces
   in Linux. If you plan to use native AX.25 interfaces, this is the preferred
   method of installation as the configuration system will attempt to limit
   the privileges available to Xastir to only those required for using native
   AX.25 interfaces, not provide full root privileges.

   Note that no user will be added to group xastir-ax25 automatically, the
   system administrator has to add them manually.

   The additional privileges are provided using the Linux Capabilities
   system where it is available and resort to setting the set-user-id bit
   of the xastir binary as a fall-back, where the Linux Capabilities system
   is not present (Debian GNU/kFreeBSD, Debian GNU/Hurd). As libax25 is not
   available on these alternative architectures, this *should* never happen.

   Linux kernels provided by Debian support Linux Capabilities, but custom
   built kernels may lack this support. If the support for Linux
   Capabilities is not present at the time of installing wireshark-common
   package, the installer will fall back to set the set-user-id bit to
   allow non-root users to use native AX.25 interfaces.

   If installation succeeds with using Linux Capabilities, non-root users
   will not be able to use native AX.25 interfaces while running kernels not
   supporting Linux Capabilities.

   The installation method can be changed any time by running:
   dpkg-reconfigure xastir
